using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using CoffeeReview.Data;
using CoffeeReview.Models;

namespace CoffeeReview.Pages.NewCafeAdmin
{
    public class DeleteModel : PageModel
    {
        private readonly CoffeeReview.Data.CoffeeCafeRpContext _context;

        public DeleteModel(CoffeeReview.Data.CoffeeCafeRpContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Cafe Cafe { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Cafe = await _context.CafeList
                .Include(c => c.Cofcafe)
                .Include(c => c.PostUser).FirstOrDefaultAsync(m => m.CafeID == id);

            if (Cafe == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Cafe = await _context.CafeList.FindAsync(id);

            if (Cafe != null)
            {
                _context.CafeList.Remove(Cafe);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
