using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using CoffeeReview.Data;
using CoffeeReview.Models;

namespace CoffeeReview.Pages.NewCafeAdmin
{
    public class CreateModel : PageModel
    {
        private readonly CoffeeReview.Data.CoffeeCafeRpContext _context;

        public CreateModel(CoffeeReview.Data.CoffeeCafeRpContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            ViewData["CoffeeCafeID"] = new SelectList(_context.CoffeeCafe, "CoffeeCafeID", "CafeType");
            ViewData["CafeMemberId"] = new SelectList(_context.Users, "Id", "Id");
            return Page();
        }

        [BindProperty]
        public Cafe Cafe { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.CafeList.Add(Cafe);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}