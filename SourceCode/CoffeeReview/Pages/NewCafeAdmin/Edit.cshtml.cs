using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CoffeeReview.Data;
using CoffeeReview.Models;

namespace CoffeeReview.Pages.NewCafeAdmin
{
    public class EditModel : PageModel
    {
        private readonly CoffeeReview.Data.CoffeeCafeRpContext _context;

        public EditModel(CoffeeReview.Data.CoffeeCafeRpContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Cafe Cafe { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Cafe = await _context.CafeList
                .Include(c => c.Cofcafe)
                .Include(c => c.PostUser).FirstOrDefaultAsync(m => m.CafeID == id);

            if (Cafe == null)
            {
                return NotFound();
            }
           ViewData["CoffeeCafeID"] = new SelectList(_context.CoffeeCafe, "CoffeeCafeID", "CafeType");
           ViewData["CafeMemberId"] = new SelectList(_context.Users, "Id", "Id");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Cafe).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CafeExists(Cafe.CafeID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool CafeExists(int id)
        {
            return _context.CafeList.Any(e => e.CafeID == id);
        }
    }
}
