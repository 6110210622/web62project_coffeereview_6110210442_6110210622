using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using CoffeeReview.Data;
using CoffeeReview.Models;

namespace CoffeeReview.Pages.NewCafeAdmin
{
    public class IndexModel : PageModel
    {
        private readonly CoffeeReview.Data.CoffeeCafeRpContext _context;

        public IndexModel(CoffeeReview.Data.CoffeeCafeRpContext context)
        {
            _context = context;
        }

        public IList<Cafe> Cafe { get;set; }
        public IList<CoffeeCafe> CoffeeCafe { get;set; }

        public async Task OnGetAsync()
        {
            Cafe = await _context.CafeList
                .Include(c => c.Cofcafe)
                .Include(c => c.PostUser).ToListAsync();

            CoffeeCafe = await _context.CoffeeCafe.ToListAsync();
        }
    }
}
