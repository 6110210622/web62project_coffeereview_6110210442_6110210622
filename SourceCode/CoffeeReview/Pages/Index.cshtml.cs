﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using CoffeeReview.Models;
using CoffeeReview.Data;

namespace CoffeeReview.Pages
{
    public class IndexModel : PageModel
    {
        private readonly CoffeeCafeRpContext db;
    	public IndexModel(CoffeeCafeRpContext db) => this.db = db;
    	public List<CoffeeCafe> coffeeCafe { get; set; } = new List<CoffeeCafe>();
        public List<Cafe> cafeList { get; set; } = new List<Cafe>();
        public List<CafeMember> cafeMember { get; set; } = new List<CafeMember>();
        public void OnGet()
        {
        	coffeeCafe = db.CoffeeCafe.ToList();
        	cafeList = db.CafeList.ToList();
            cafeMember = db.CafeMember.ToList();
        }
    }
}
