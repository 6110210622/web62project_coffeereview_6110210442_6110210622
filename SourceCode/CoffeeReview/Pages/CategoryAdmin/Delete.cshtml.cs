using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using CoffeeReview.Data;
using CoffeeReview.Models;

namespace CoffeeReview.Pages.CategoryAdmin
{
    public class DeleteModel : PageModel
    {
        private readonly CoffeeReview.Data.CoffeeCafeRpContext _context;

        public DeleteModel(CoffeeReview.Data.CoffeeCafeRpContext context)
        {
            _context = context;
        }

        [BindProperty]
        public CoffeeCafe CoffeeCafe { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            CoffeeCafe = await _context.CoffeeCafe.FirstOrDefaultAsync(m => m.CoffeeCafeID == id);

            if (CoffeeCafe == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            CoffeeCafe = await _context.CoffeeCafe.FindAsync(id);

            if (CoffeeCafe != null)
            {
                _context.CoffeeCafe.Remove(CoffeeCafe);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
