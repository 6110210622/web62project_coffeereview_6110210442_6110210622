using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using CoffeeReview.Data;
using CoffeeReview.Models;

namespace CoffeeReview.Pages.CategoryAdmin
{
    public class IndexModel : PageModel
    {
        private readonly CoffeeReview.Data.CoffeeCafeRpContext _context;

        public IndexModel(CoffeeReview.Data.CoffeeCafeRpContext context)
        {
            _context = context;
        }

        public IList<CoffeeCafe> CoffeeCafe { get;set; }

        public async Task OnGetAsync()
        {
            CoffeeCafe = await _context.CoffeeCafe.ToListAsync();
        }
    }
}
