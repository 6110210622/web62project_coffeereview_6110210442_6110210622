using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CoffeeReview.Data;
using CoffeeReview.Models;

namespace CoffeeReview.Pages.CategoryAdmin
{
    public class EditModel : PageModel
    {
        private readonly CoffeeReview.Data.CoffeeCafeRpContext _context;

        public EditModel(CoffeeReview.Data.CoffeeCafeRpContext context)
        {
            _context = context;
        }

        [BindProperty]
        public CoffeeCafe CoffeeCafe { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            CoffeeCafe = await _context.CoffeeCafe.FirstOrDefaultAsync(m => m.CoffeeCafeID == id);

            if (CoffeeCafe == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(CoffeeCafe).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CoffeeCafeExists(CoffeeCafe.CoffeeCafeID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool CoffeeCafeExists(int id)
        {
            return _context.CoffeeCafe.Any(e => e.CoffeeCafeID == id);
        }
    }
}
