using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using CoffeeReview.Data;
using CoffeeReview.Models;

namespace CoffeeReview.Pages.CategoryAdmin
{
    public class CreateModel : PageModel
    {
        private readonly CoffeeReview.Data.CoffeeCafeRpContext _context;

        public CreateModel(CoffeeReview.Data.CoffeeCafeRpContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public CoffeeCafe CoffeeCafe { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.CoffeeCafe.Add(CoffeeCafe);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}