using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;
namespace CoffeeReview.Models
{
    public class CafeMember : IdentityUser{
            public string FirstName { get; set;}
            public string LastName { get; set;}
            }

    public class CoffeeCafe {
        public int CoffeeCafeID { get; set;}
        public string CafeType {get; set;}
        public string ReviewType { get; set;}
        }
    public class Cafe {
        public int CafeID { get; set; }
        public int CoffeeCafeID { get; set; }
        public CoffeeCafe Cofcafe { get; set; }

        [DataType(DataType.Date)]
        public string ReviewDate { get; set; }
        public string NameCafe { get; set; }
        public string Openingtime { get; set; }
        public string Contactnumber { get; set; }
        public string Review { get; set; }
        public string imageReview { get; set; }
        public float GpsLat { get; set; }
        public float GpsLng { get; set; }
        public int ReviewScore { get; set; }
        
        public string CafeMemberId {get; set;}
        public CafeMember PostUser {get; set;}
        }
        
}