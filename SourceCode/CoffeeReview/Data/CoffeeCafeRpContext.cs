using CoffeeReview.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace CoffeeReview.Data
{
    public class CoffeeCafeRpContext : IdentityDbContext<CafeMember>
    {
        public DbSet<Cafe> CafeList { get; set; }
        public DbSet<CoffeeCafe> CoffeeCafe { get; set; }
        public DbSet<CafeMember> CafeMember { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        optionsBuilder.UseSqlite(@"Data source=CoffeeCafeRp.db");
        }
    }
}
